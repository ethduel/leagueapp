# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ethduel,
  ecto_repos: [Ethduel.Repo]

# Configures the endpoint
config :ethduel, Ethduel.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Jo3lTgor7Bc5RqeUf042hXOuWrfpmIypdWXYjlr2zc/0ZRsOGCJgnxOVTtrY7TGL",
  render_errors: [view: Ethduel.Web.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ethduel.PubSub,
           adapter: Phoenix.PubSub.PG2],
  instrumenters: [Ethduel.PhoenixInstrumenter]

# Import env vars
config :ethduel,
  riot_api_key:  {:system, "RIOT_API_KEY", "riot_key"}

#cronjob scheduler
config :ethduel, Ethduel.Scheduler,
  global: true,
  overlap: true,
  jobs: [
    # Every 2 minutes check DB for finished games and resolve bets
    # "*/2 * * * *": : {Ethduel.Transaction, :resolve_bets}
    # Every 6 minutes update the conversion prices for Eth
     {"*/6 * * * *", {Ethduel.Util, :putCurrencyConversionsInCache, []}}

  ]

# Prometheus configs
config :prometheus, Ethduel.PhoenixInstrumenter,  
  controller_call_labels: [:controller, :action],
  duration_buckets: [10, 25, 50, 100, 250, 500, 1000, 2500, 5000,
                   10_000, 25_000, 50_000, 100_000, 250_000, 500_000,
                   1_000_000, 2_500_000, 5_000_000, 10_000_000],
  registry: :default,
  duration_unit: :microseconds

config :prometheus, Ethduel.PipelineInstrumenter,  
  labels: [:status_class, :method, :host, :scheme, :request_path],
  duration_buckets: [10, 100, 1_000, 10_000, 100_000,
                   300_000, 500_000, 750_000, 1_000_000,
                   1_500_000, 2_000_000, 3_000_000],
  registry: :default,
  duration_unit: :microseconds

# Configures Elixir's Logger
config :logger, :console,
  format: "$date $time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Import Timber, structured logging
import_config "timber.exs"
