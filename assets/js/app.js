// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import 'phoenix_html';

import $ from "jquery"
import "jquery"
import "bootstrap-select"
global.jQuery = require("jquery")
global.bootstrap = require("bootstrap")

//Import local files
import MainView from './views/main';
import socket from "./socket";
import Game from "./game";
import Users from "./users";
import Wallet from "./wallet";


function handleDOMContentLoaded() {
  const view = new MainView();
  view.mount();
  window.currentView = view;
}
function handleDocumentUnload() {
  window.currentView.unmount();
}
window.addEventListener('DOMContentLoaded', handleDOMContentLoaded, false);
window.addEventListener('unload', handleDocumentUnload, false);

// Check web3 and find out what network we're on, then start the Channels
window.addEventListener('load', function() {
    // Check if Web3 has been injected by the browser:
  if (typeof web3 !== 'undefined' && typeof web3 !== 'loading') {
    window.web3 = new Web3(web3.currentProvider);
    window.MetaMaskBool = true;
    Users.init(socket);  //will start a wallet:* channel if MetaMask connected

  } else {
      window.MetaMaskBool = false;
      console.log("web3 provider problem");
      Users.updateUI(MetaMaskBool);
    }
    if (window.location.pathname.split("/")[1] === 'game') {
      Game.init(socket);// let the MetaMask wallet load if its there
    }
    
  })

  // Default provider for TestRPC
  const provider = new Web3.providers.HttpProvider('http://localhost:8545')









window.addEventListener('load', function() {
  //check if it's the first page load in 24 hours
    if (document.cookie.replace(/(?:(?:^|.*;\s*)sawLegalModal\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
      $('#legalModal').modal('show');
      createCookie("sawLegalModal","true",24); //expires in 24 hours
    }
})

function createCookie(name,value,hours) {
  if (hours) {
      var date = new Date();
      date.setTime(date.getTime()+(hours*60*60*1000));        
      var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}


