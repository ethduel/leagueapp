defmodule Ethduel.Wager do
  use Ecto.Schema
  import Ecto.Changeset
  alias Ethduel.Wager



  schema "wagers" do
    field :amount, :decimal, precision: 26 # in Wei
    field :winning_team, :string #can be "blue" or "red"
    field :blue_wallet, :string
    field :red_wallet, :string
    field :game_remake, :boolean
    field :paid, :boolean, default: false
 
    # associations
    belongs_to :riot_game, Ethduel.Game,  references: :riot_game_id, type: :decimal

    #only store association for winning wallet for now, can't figure out how to do this double association to 2 wallets
    belongs_to :wallet, Ethduel.Wallet, references: :address, foreign_key: :address, primary_key: true, type: :string

    timestamps()
  end


     @doc false
  def changeset(%Wager{} = wager, attrs) do
    wager
    |> cast(attrs, [:amount, :winning_team, :losing_team, :blue_wallet, :red_wallet, :winning_wallet, :losing_wallet, :game_remake, :paid])
    |> validate_required([:amount, :blue_wallet, :red_wallet])
    end
end
